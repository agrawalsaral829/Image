Flask==3.0.3
flask-cors
opencv-python==4.9.0.80
opencv-python-headless==4.8.0.74
numpy==1.26.2
Werkzeug==3.0.1
tensorflow-cpu
gunicorn
