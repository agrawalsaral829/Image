import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, request, jsonify
from werkzeug.utils import secure_filename
import os
import cv2
import numpy as np
from tensorflow.keras.models import load_model
import contextlib
from flask_cors import CORS
import base64

app = Flask(__name__)
CORS(app)


# Load the model
model = load_model('butterflies_vgg16_model_tl.h5')

# Define the list of class names
class_names = ["Danaus_Plexippus", "Heliconius_Charitonius", "Heliconius_Erato", "Junonia_Coenia", "Lycaena_Phlaeas", "Nymphalis_Antiopa", "Papilio_Cresphontes", "Pieris_Rapae", "Vanessa_Atalanta", "Vanessa_Cardui"]

# Function to predict and visualize single image
def predict_and_visualize_single_image(image_path, model, class_names):
    try:
        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = cv2.resize(image, (256, 256)).astype(np.float32) / 255.0
        image = np.expand_dims(image, axis=0)
        with open(os.devnull, "w") as f, contextlib.redirect_stdout(f), contextlib.redirect_stderr(f):
            prediction = model.predict(image)
        predicted_index = np.argmax(prediction)
        predicted_class = class_names[predicted_index]
        probability = prediction[0][predicted_index]
        return predicted_class, probability, image[0]
    except Exception as e:
        logger.error(f"Error processing image: {e}")
        raise

@app.route('/predict', methods=['POST'])
def predict():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'}), 400
    
    f = request.files['file']
    if f.filename == '':
        return jsonify({'error': 'No selected file'}), 400

    filepath = os.path.join('static', secure_filename(f.filename))
    try:
        f.save(filepath)
        predicted_class, probability, predicted_image = predict_and_visualize_single_image(filepath, model, class_names)
        
        # Encode the image in base64
        _, buffer = cv2.imencode('.jpg', cv2.cvtColor(predicted_image * 255, cv2.COLOR_RGB2BGR))
        image_base64 = base64.b64encode(buffer).decode('utf-8')
        
        os.remove(filepath)
        prediction_text = f'Predicted butterfly class is "{predicted_class}" with probability {probability:.4f}'
        return jsonify({'prediction_text': prediction_text, 'image_base64': image_base64})
    except Exception as e:
        logger.error(f"Error during prediction: {e}")
        return jsonify({'error': 'Internal server error'}), 500
    finally:
        if os.path.exists(filepath):
            os.remove(filepath)    

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)